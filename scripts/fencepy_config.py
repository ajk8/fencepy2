import fencepy_core
import os

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

CONFIG_PATH = os.path.join(fencepy_core.FENCEPY_ROOT_DIR, 'config.ini')
DEFAULT_CONFIG = {
    'standard_requirements': []
}


class FencepyConfig(object):

    def __init__(self, path=None):
        self.path = path or CONFIG_PATH
        self._config = None

    def load(self):
        if not os.path.isfile(self.path):
            self._config = {}
        else:
            parser = configparser.ConfigParser()
            parser.read(self.path)
            for key, value in parser.items(section='DEFAULT'):
                if key not in DEFAULT_CONFIG.keys():
                    raise KeyError('bad key found in {}: {}'.format(self.path, key))
                if not isinstance(value, type(DEFAULT_CONFIG[key])):
                    raise TypeError('expected {} to be of type {}, got {}'.format(
                        key, type(DEFAULT_CONFIG[key]), type(value)))
                self._config[key] = value
            for key, value in DEFAULT_CONFIG:
                if key not in self._config.keys():
                    self._config[key] = value

    def _load_once(self):
        if self._config is None:
            self.load()

    def get(self, key):
        self._load_once()
        if key not in self._config.keys():
            raise KeyError('{} does not appear to be a valid configuration key'.format(key))
        return self._config[key]


fpconfig = FencepyConfig()
