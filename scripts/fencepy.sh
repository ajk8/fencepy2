FENCEPY_SCRIPTS_ROOT=$(python -c"import os; print(os.path.dirname(os.path.realpath('$0')))")

fencepy-create() { python $FENCEPY_SCRIPTS_ROOT/fencepy_create.py $@ }

fpnew() { fencepy-create $@ }

fencepy-erase() {
    python $FENCEPY_SCRIPTS_ROOT/fencepy_erase.py $@
}

fpdel() { fencepy-erase $@ }

fencepy-update() { python $FENCEPY_SCRIPTS_ROOT/fencepy_update.py $@ }

fpup() { fencepy-update $@ }

fencepy-activate() {
    virtualenv_path=$(python $FENCEPY_SCRIPTS_ROOT/fencepy_print_virtualenv_path.py $@)
    test $? -ne 0 && return 1
    activate_path=$virtualenv_path/bin/activate
    source $activate_path || return 1
    return 0
}

fpsrc() { fencepy-activate $@ }
