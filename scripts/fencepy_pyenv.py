import subprocess
import shlex
import os

_PYENV_ENV = os.environ.copy()

VERSION_HELP = '''
To find out which versions of python are currently installed in pyenv, execute:

    pyenv versions

To find out which versions of python can be installed into pyenv, execute:

    pyenv install --list

To install a new version into pyenv, execute:

    pyenv install <python_version>
'''


class VersionError(RuntimeError):
    help_text = VERSION_HELP
    pass


def python_version_exists(python_version):
    """ Check to see if <python_version> shows up in the output from `pyenv versions` """
    versions_output = subprocess.check_output(
        shlex.split('pyenv versions --bare --skip-aliases'),
        stderr=open(os.devnull)
    ).decode()
    return python_version in versions_output.splitlines()


def set_python_version(python_version, sticky_path=None):
    """ Set the variable that pyenv likes for in-the-moment versioning and also
        set a flag to be clear that we've done the override. If <python_version>
        is not passed in, set the version to whatever pyenv is already using.
        Return the contents of that variable.
    """
    if not python_version_exists(python_version):
        raise VersionError('cannot set <python_version> to "{}"'.format(python_version))
    _PYENV_ENV['PYENV_VERSION'] = python_version
    if sticky_path is not None:
        subprocess.call(
            shlex.split('pyenv local ' + python_version),
            cwd=sticky_path
        )


def get_python_version():
    """ Return the usable output from `pyenv version` """
    version_output = subprocess.check_output(
        shlex.split('pyenv version'),
        stderr=open(os.devnull),
        env=_PYENV_ENV
    ).decode()
    return version_output.split()[0]


def get_pyenv_root():
    """ Return the path to the directory where pyenv files are stored """
    root_output = subprocess.check_output(
        shlex.split('pyenv root'),
        stderr=open(os.devnull),
        env=_PYENV_ENV
    ).decode()
    return root_output.strip()


def get_virtualenv_prefix(virtualenv_name):
    """ Return the usable output from `pyenv prefix <virtualenv_name>` """
    virtualenv_prefix_output = subprocess.check_output(
        shlex.split('pyenv prefix ' + virtualenv_name),
        stderr=open(os.devnull),
        env=_PYENV_ENV
    ).decode()
    return virtualenv_prefix_output.strip()


def virtualenv_exists(virtualenv_name):
    """ Check to see if <virtualenv_name> shows up in the output from `pyenv virtualenvs` """
    pyenv_root = get_pyenv_root()
    return virtualenv_name in os.listdir(os.path.join(pyenv_root, 'versions'))
    # `pyenv virtualenvs` is quite slow, hence the short-circuit here
    # virtualenvs_output = subprocess.check_output(
    #     shlex.split('pyenv virtualenvs'),
    #     stderr=open(os.devnull),
    #     env=_PYENV_ENV
    # ).decode()
    # return virtualenv_name in virtualenvs_output


def create_virtualenv(virtualenv_name):
    """ Shell out to `pyenv virtualenv <virtualenv_name>` """
    subprocess.call(
        shlex.split('pyenv virtualenv ' + virtualenv_name),
        env=_PYENV_ENV
    )


def delete_virtualenv(virtualenv_name):
    """ Shell out to `pyenv virtualenv-delete <virtualenv_name> """
    subprocess.call(
        shlex.split('pyenv virtualenv-delete --force ' + virtualenv_name),
        env=_PYENV_ENV
    )
