import fencepy_core


def run(project_path):
    fencepy_core.log('making sure pip is up to date')

    cmd_result = fencepy_core.get_cmd_result(fencepy_core.get_virtualenv_path(project_path) +
                                             '/bin/pip install --upgrade pip')
    if cmd_result.retval != 0:
        fencepy_core.log('failed to update pip', log_level=fencepy_core.LOG_LEVEL.ERROR)
        return 1
    if any(['already up-to-date' in l for l in cmd_result.stdout]):
        fencepy_core.log('pip was already up to date')
        return -1
    fencepy_core.log('pip is now up to date', log_level=fencepy_core.LOG_LEVEL.SUCCESS)
    return 0
