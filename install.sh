which pyenv >/dev/null 2>&1 || cat <<HERE

    ### NOTICE ###

    This software is a wrapper around pyenv, and won't actually
    function without having pyenv installed. You are seeing this
    message because pyenv is not on your PATH. If you don't have
    it installed, see:

        https://github.com/yyuu/pyenv-installer

    If you do, please ensure that it is on the PATH before you
    try to use fencepy2. Now, back to your regularly scheduled
    install script.

HERE

GIT_CLONE_URL='https://gitlab.com/ajk8/fencepy2.git'
USER_HOME=$(echo ~)
FENCEPY_HOME="$USER_HOME/.fencepy2"

# clone source if necessary
no_download_flag=$(test -f .gitignore && test -d scripts)
if $no_download_flag; then
    FENCEPY_HOME=`pwd`
else
    test -d $FENCEPY_HOME && rm -rf $FENCEPY_HOME
    git clone $GIT_CLONE_URL $FENCEPY_HOME
    cd $FENCEPY_HOME
fi

INIT_SCRIPT="$FENCEPY_HOME/scripts/fencepy.sh"

# look for oh-my-zsh
oh_my_flag=$(test -d ~/.oh-my-zsh)
if $oh_my_flag; then
    zsh_d_path="$USER_HOME/.oh-my-zsh/custom/fencepy.zsh"
    test -f $zsh_d_path && echo "[FENCEPY] it appears that you have oh-my-zsh script" \
                                "from the original fencepy installed!" && \
                           echo "[FENCEPY] please remove $zsh_d_path before continuing" >&2 && \
                           exit 1
    test -L $zsh_d_path && rm -f $zsh_d_path
    ln -s $INIT_SCRIPT $zsh_d_path || exit 1
    echo "[FENCEPY] configured oh-my-zsh, reload using 'source ~/.zshrc'" >&2
fi

# TODO: add installation for other shells
