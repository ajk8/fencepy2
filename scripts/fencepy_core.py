import subprocess
import shlex
import os
import sys
import argparse
import fencepy_pyenv

SHELL_DEPENDENCIES = ['pyenv']
FENCEPY_ROOT_DIR = os.path.join(os.path.expanduser('~'), '.fencepy2')


class LoggingEnum(object):
    LOG = WHITE = '\033[37m'
    SUCCESS = GREEN = '\033[32m'
    FAILURE = ERROR = RED = '\033[31m'
    RESET = '\033[39m'


LOG_LEVEL = LoggingEnum


def log(msg, log_level=LOG_LEVEL.WHITE, skip_prefix=False):
    prefix = '[FENCEPY] ' if not skip_prefix else ''
    line = log_level + prefix + msg + LOG_LEVEL.RESET
    sys.stderr.write(line + os.linesep)
    sys.stderr.flush()


def summary(msg, virtualenv_name, log_level=LOG_LEVEL.WHITE):
    summary = '"{}" {}, fencepy out!'.format(virtualenv_name, msg)
    log(summary, log_level)


def check_dependencies():
    for dependency in SHELL_DEPENDENCIES:
        try:
            devnull = open(os.devnull, 'w')
            subprocess.check_call(shlex.split('which ' + dependency),
                                  stdout=devnull, stderr=devnull)
        except subprocess.CalledProcessError:
            log('could not find dependency on the PATH: ' + dependency, log_level=LOG_LEVEL.ERROR)
            return False
    return True


def get_project_root(input_dir=None):
    if input_dir is None:
        input_dir = os.getcwd()
    try:
        git_root = subprocess.check_output(
            shlex.split('git rev-parse --show-toplevel'.format(input_dir)),
            cwd=input_dir,
            stderr=open(os.devnull)
        ).decode()
        return git_root.strip()
    except subprocess.CalledProcessError:
        return input_dir


def get_virtualenv_name(project_path):

    python_version_str = fencepy_pyenv.get_python_version()

    # if we're one directory below the root, this logic needs to work differently
    parent = os.path.dirname(project_path)
    if parent in ('/', os.path.splitdrive(parent)[0]):
        return '-'.join((os.path.basename(project_path), python_version_str))

    else:
        # need the realpath here because in some circumstances windows paths get passed
        # with a '/' and others see it coming in as a '\'
        tokens = os.path.dirname(os.path.realpath(project_path)).split(os.path.sep)
        tokens.reverse()
        if tokens[-1] == '':
            tokens = tokens[:-1]
        prjpart = '.'.join([os.path.basename(project_path), '.'.join([d[0] for d in tokens])])
        return '-'.join((prjpart, python_version_str))


def virtualenv_exists(project_path):
    virtualenv_name = get_virtualenv_name(project_path)
    return fencepy_pyenv.virtualenv_exists(virtualenv_name)


def get_virtualenv_path(project_path):
    if not virtualenv_exists(project_path):
        return None
    virtualenv_name = get_virtualenv_name(project_path)
    virtualenv_prefix = fencepy_pyenv.get_virtualenv_prefix(virtualenv_name)
    return virtualenv_prefix


PARSEABLE_ARG_NAMES = ('python_version', 'sticky')


def parse_args(*arg_names):
    for arg_name in arg_names:
        if arg_name not in PARSEABLE_ARG_NAMES:
            raise ValueError(arg_name + ' not a valid arg_name for parse_args')
    parser = argparse.ArgumentParser()
    if 'python_version' in arg_names:
        parser.add_argument('python_version', nargs='?', default=None,
                            help='python version to use when definining the virtualenv')
    if 'sticky' in arg_names:
        parser.add_argument('-S', '--sticky', action='store_true', default=False,
                            help='make version passed as python_version the new default')
    return parser.parse_args()


class CmdResult(object):
    def __init__(self):
        self.retval = None
        self.stdout = []
        self.stderr = []


def get_cmd_result(cmd):
    p = subprocess.Popen(
        shlex.split(cmd),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
    )
    cmd_result = CmdResult()
    cmd_result.retval = p.poll()
    while cmd_result.retval is None:
        for key in ('stdout', 'stderr'):
            line = getattr(p, key).readline()
            while line:
                getattr(sys, key).write(line)
                getattr(sys, key).flush()
                getattr(cmd_result, key).append(line.strip())
                line = getattr(p, key).readline()
        cmd_result.retval = p.poll()
    p.stdout.close()
    p.stderr.close()
    return cmd_result
