import fencepy_core
import importlib
import os
import re

PLUGIN_FILENAME_REGEX = re.compile(r'^plugin_(\w+).py$')


class FencepyPlugins(object):
    def __init__(self):
        self._plugins = None

    def load(self):
        self._plugins = {}
        for filename in os.listdir(os.path.dirname(__file__)):
            match = PLUGIN_FILENAME_REGEX.match(filename)
            if match is not None:
                self._plugins[match.group(1)] = importlib.import_module(filename[:-3])

    def _load_once(self):
        if self._plugins is None:
            self.load()

    def get_module(self, plugin_name):
        self._load_once()
        if plugin_name not in self._plugins.keys():
            raise KeyError('"{}" is not a valid plugin name'.format(plugin_name))
        return self._plugins[plugin_name]

    def get_names(self):
        self._load_once()
        return self._plugins.keys()


fpplugins = FencepyPlugins()


def execute(plugin_name, project_path):
    plugin_module = fpplugins.get_module(plugin_name)
    fencepy_core.log('executing plugin: ' + plugin_name)
    return plugin_module.run(project_path)


def execute_all(project_path):
    retval_list = []
    for plugin_name in fpplugins.get_names():
        retval_list.append(execute(plugin_name, project_path))
    return retval_list
