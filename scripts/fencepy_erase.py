import fencepy_core
import fencepy_pyenv


def main(python_version=None):
    if not fencepy_core.check_dependencies():
        return 1
    if python_version is not None:
        try:
            fencepy_pyenv.set_python_version(python_version)
        except fencepy_pyenv.VersionError as e:
            fencepy_core.log(e.message, log_level=fencepy_core.LOG_LEVEL.ERROR)
            fencepy_core.log(e.help_text, skip_prefix=True)
            return 1
    project_path = fencepy_core.get_project_root()
    virtualenv_name = fencepy_core.get_virtualenv_name(project_path)
    if not fencepy_core.virtualenv_exists(project_path):
        fencepy_core.summary('virtualenv does not exist', virtualenv_name,
                             log_level=fencepy_core.LOG_LEVEL.ERROR)
        return 1
    fencepy_pyenv.delete_virtualenv(virtualenv_name)
    fencepy_core.summary('virtualenv erased successfully', virtualenv_name,
                         log_level=fencepy_core.LOG_LEVEL.SUCCESS)
    return 0


if __name__ == '__main__':
    parsed = fencepy_core.parse_args('python_version')
    raise SystemExit(main(parsed.python_version))
