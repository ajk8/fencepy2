import fencepy_core
import fencepy_plugins
import fencepy_pyenv


def main(python_version=None, sticky=False):
    if not fencepy_core.check_dependencies():
        return 1
    project_path = fencepy_core.get_project_root()
    if python_version is not None:
        try:
            fencepy_pyenv.set_python_version(
                python_version, sticky_path=project_path if sticky else None)
        except fencepy_pyenv.VersionError as e:
            fencepy_core.log(e.message, log_level=fencepy_core.LOG_LEVEL.ERROR)
            fencepy_core.log(e.help_text, skip_prefix=True)
            return 1
    virtualenv_name = fencepy_core.get_virtualenv_name(project_path)
    if not fencepy_core.virtualenv_exists(project_path):
        fencepy_core.summary('virtualenv does not exist, create it with fencepy-create',
                             virtualenv_name, log_level=fencepy_core.LOG_LEVEL.ERROR)
        return 1
    ret_list = fencepy_plugins.execute_all(project_path)
    if max(ret_list) == -1:
        fencepy_core.summary('no plugins took any action', virtualenv_name)
    else:
        fencepy_core.summary('virtualenv updated successfully', virtualenv_name,
                             log_level=fencepy_core.LOG_LEVEL.SUCCESS)
    return 0


if __name__ == '__main__':
    parsed = fencepy_core.parse_args('python_version', 'sticky')
    raise SystemExit(main(parsed.python_version, parsed.sticky))
