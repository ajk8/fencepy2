import fencepy_core
import os


def run(project_path):
    requirements_txt_path = os.path.join(project_path, 'requirements.txt')
    if not os.path.exists(requirements_txt_path):
        return -1
    fencepy_core.log('installing project requirements')
    cmd_result = fencepy_core.get_cmd_result(fencepy_core.get_virtualenv_path(project_path) +
                                             '/bin/pip install -r {}'.format(requirements_txt_path))
    if cmd_result.retval != 0:
        fencepy_core.log('failed to install project requirements',
                         log_level=fencepy_core.LOG_LEVEL.ERROR)
        return 1
    if all(['already satisfied' in l for l in cmd_result.stdout]):
        fencepy_core.log('all project requirements already installed')
        return -1
    fencepy_core.log('project requirements installed successfully',
                     log_level=fencepy_core.LOG_LEVEL.SUCCESS)
    return 0
