import fencepy_core
import fencepy_pyenv
from fencepy_cache import fpcache
import json
import os


def run(project_path):
    # if there's a sublime-project file in the home directory, then don't waste time
    # searching the user's home directory
    sublime_config_path = None
    for filename in os.listdir(project_path):
        if filename.endswith('.sublime-project'):
            sublime_config_path = os.path.join(project_path, filename)
            break

    if sublime_config_path is None:
        fencepy_core.log('could not find .sublime-project file inside project, searching ~/')
        project_name = os.path.basename(project_path)
        sublime_config_name = project_name + '.sublime-project'

        # be smart about this, keep a persistent cache of searched directories
        # and look there first
        cache_key = 'plugin_sublime_project_paths'
        cached_paths = fpcache.get(cache_key, [])
        for cached_path in cached_paths:
            if os.path.isdir(cached_path) and sublime_config_name in os.listdir(cached_path):
                sublime_config_path = os.path.join(cached_path, sublime_config_name)
        if sublime_config_path is None:
            for root, dirnames, filenames in os.walk(os.path.expanduser('~')):
                if sublime_config_name in filenames:
                    sublime_config_path = os.path.join(root, sublime_config_name)
                    cached_paths.append(root)
                    fpcache.upsert(cache_key, cached_paths)
                    fpcache.dump()
                    break
        if sublime_config_path is None:
            fencepy_core.log('could not find any file called ' + sublime_config_name)
            return -1

    sublime_config_dict = json.load(open(sublime_config_path))
    virtualenv_name = fencepy_core.get_virtualenv_name(project_path)
    virtualenv_path = fencepy_pyenv.get_virtualenv_prefix(virtualenv_name)
    python_bin = os.path.join(virtualenv_path, 'bin', 'python')

    changed = False
    if 'settings' not in sublime_config_dict.keys():
        sublime_config_dict['settings'] = {}
    if 'python_interpreter' not in sublime_config_dict['settings']:
        sublime_config_dict['settings']['python_interpreter'] = ''
    if sublime_config_dict['settings']['python_interpreter'] != python_bin:
        sublime_config_dict['settings']['python_interpreter'] = python_bin
        changed = True

    shell_cmd = '"{}" -u "$file"'.format(python_bin)
    if 'build_systems' in sublime_config_dict.keys():
        for build_system_dict in sublime_config_dict['build_systems']:
            if build_system_dict.get('name', '').startswith('Anaconda'):
                if build_system_dict.get('shell_cmd', '') != shell_cmd:
                    build_system_dict['shell_cmd'] = shell_cmd
                    changed = True

    if not changed:
        fencepy_core.log(sublime_config_path + ' already configured to use virtualenv')
        return -1

    json.dump(sublime_config_dict, open(sublime_config_path, 'w'),
              indent=4, separators=(', ', ': '), sort_keys=True)
    fencepy_core.log(sublime_config_path + ' configured to use virtualenv successfully',
                     log_level=fencepy_core.LOG_LEVEL.SUCCESS)
    return 0
