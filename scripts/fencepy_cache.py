import fencepy_core
import json
import os

CACHE_PATH = os.path.join(fencepy_core.FENCEPY_ROOT_DIR, 'cache.json')


class FencepyCache(object):

    def __init__(self, path=None):
        self.path = path or CACHE_PATH
        self._cache = None

    def load(self):
        if not os.path.isfile(self.path):
            self._cache = {}
        else:
            with open(self.path, 'r') as cache_file:
                self._cache = json.load(cache_file)

    def dump(self):
        cache_parent = os.path.dirname(self.path)
        if not os.path.isdir(cache_parent):
            os.makedirs(cache_parent)
        with open(self.path, 'w') as cache_file:
            json.dump(self._cache, cache_file)

    def _load_once(self):
        if self._cache is None:
            self.load()

    def has(self, key):
        self._load_once()
        return key in self._cache.keys()

    def get(self, key, miss_value='IF_NOT_SET_RAISE_KEYERROR_ON_MISS'):
        self._load_once()
        if self.has(key):
            return self._cache[key]
        elif miss_value == 'IF_NOT_SET_RAISE_KEYERROR_ON_MISS':
            raise KeyError(key)
        return miss_value

    def upsert(self, key, value):
        self._load_once()
        self._cache[key] = value


fpcache = FencepyCache()
